interface ScrambleOptions {
    turns: number;
    array: boolean;
}
export default function scramble(options?: Partial<ScrambleOptions>): string | string[];
export {};

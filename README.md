# rubiks-cube-scramble

A Rubiks Cube Scramble Generator

## Installation

```sh
npm install rubiks-cube-scramble
yarn add rubiks-cube-scramble
```

## Usage

```javascript
import scrambleGenerator from "rubiks-cube-scramble";

export const Scrambler = () => {
  const [scramble, setScramble] = useState();

  const handleScramble = () => {
    setScramble(scrambleGenerator());
  };

  return (
    <div>
      <strong>{scramble}</strong>
      <button onClick={() => handleScramble}>Scramble!</button>
    </div>
  );
};
```

```sh
Output: "D F2 B2 D2 R' B F' R L U2 F U R' B L B' F2 D F R'"
```

## Options

```javascript
import scrambleGenerator from "rubiks-cube-scramble";

const scramble;

// generate scramble with 30 turns
scramble = scrambleGenerator({ turns: 30 });
// generate scramble in array type
scramble = scrambleGenerator({ array: true });
```
